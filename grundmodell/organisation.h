#ifndef ORGANISATION_H
#define ORGANISATION_H

#include <QObject>
#include <gemensamt/serializable.h>


class Organisation : public QObject, public Serializable
{
        Q_OBJECT
    public:
        explicit Organisation(QVariantHash object, QObject *parent = nullptr);
        explicit Organisation(const QString &filePath, QObject *parent = nullptr);

        static bool isThisObject(QVariantHash object);
        virtual QVariantHash serialize() const override;
        void deserialize(QVariantHash object) override;

        static Organisation* readFrom(const QString& filePath);

    public slots:
        void saveTo(const QString& filePath);

    signals:

    protected:

        QString                 mNamn;

};

#endif // ORGANISATION_H
