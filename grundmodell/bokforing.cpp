#include "bokforing.h"

#include <QVariant>
#include <QVariant>
#include <QFile>
#include <QJsonDocument>
#include <QDir>
#include <QString>

#include <exceptions/objectinitializationfailedexception.h>

#include "bokforingsar.h"
#include "organisation.h"

#include <QtDebug>

#include <exceptions/failedtoreadjsonfileexception.h>

Bokforing::Bokforing(QVariantHash object, const QString &basePath, QObject *parent)
    : QObject{parent}
    , Serializable{QDir(basePath).filePath("Inställningar/Bokföring.json") }
    , mBasePath{basePath}
    , mOrgansation{ new Organisation(mBasePath.filePath("Inställningar/Organisation.json"))}
{
    deserialize(object);
}

Bokforing::Bokforing(const QString &basePath, QObject *parent)
    : QObject{parent}
    , Serializable{QDir(basePath).filePath("Inställningar/Bokföring.json") }
    , mBasePath{basePath}
    , mOrgansation{ new Organisation(mBasePath.filePath("Inställningar/Organisation.json"))}
{

    deserialize(readJsonDocument("Bokföring").toVariant().toHash());
}

Momskoder *Bokforing::momskoder() const
{
    if (mAktuelltBokforingsAr)
        return mAktuelltBokforingsAr->momskoder();
    else
        return nullptr;
}

Kontoplan *Bokforing::kontoplan() const
{
    if (mAktuelltBokforingsAr)
        return mAktuelltBokforingsAr->kontoplan();
    else
        return nullptr;

}

bool Bokforing::isThisObject(QVariantHash object)
{
    if (object.contains("Object") && object.value("Object") == "Bokföring")
        return true;

    return false;

}

QVariantHash Bokforing::serialize() const
{
    QVariantHash result;
    QVariantList    bokforingsar;

    result.insert("Object", "Bokföring");
    result.insert("Öppnat år", mAktuelltBokforingsAr->ar());

    foreach(BokforingsAr* ar, mBokforingsAr)
    {
        bokforingsar.append(ar->ar());
    }

    result.insert("Bokföringsår", bokforingsar);

    return result;
}

void Bokforing::deserialize(QVariantHash object)
{
    if (isThisObject(object))
    {

        foreach(QVariant vBokforingsar, object.value("Bokföringsår").toList())
        {
//            BokforingsAr* bokforingsar = BokforingsAr::readFrom(mBasePath.filePath(vBokforingsar.toString()));
            BokforingsAr* bokforingsar = new BokforingsAr(mBasePath.filePath(vBokforingsar.toString()), this);
            mBokforingsAr.insert(bokforingsar->ar(), bokforingsar);
            if (object.value("Öppnat år").toString() == vBokforingsar.toString())
                mAktuelltBokforingsAr = bokforingsar;

        }

        return;

    }

    throw ObjectInitializationFailedException("Failed to initialize Bokforing from QVariantHash",
                                              "Bokforing::initialize(QVariantHash object)");

}

void Bokforing::save()
{
    qDebug() << "Sparar bokföring under " << mBasePath;

    saveJsonDocument(QJsonDocument::fromVariant(serialize()),
                     filePath(),
                     "Bokföring");

    foreach(BokforingsAr* ar, mBokforingsAr)
    {
        ar->saveTo(mBasePath.path());
    }
}

void Bokforing::saveTo(const QString &basePath)
{

    qDebug() << "Sparar bokföring under " << basePath;

    saveJsonDocument(QJsonDocument::fromVariant(serialize()),
                     QDir(basePath).filePath("Inställningar/Bokföring.json"),
                     "Bokföring");

    mOrgansation->saveTo(QDir(basePath).filePath("Inställningar/Organisation.json"));

    foreach(BokforingsAr* ar, mBokforingsAr)
    {
        QDir base(basePath);
        ar->saveTo(base.filePath(QString::number(ar->ar())));
    }

}
