#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

#include <kontomodell/kontoplan.h>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class Bokforing;

class MainWindow : public QMainWindow
{
		Q_OBJECT

	public:
		MainWindow(QWidget *parent = nullptr);
		~MainWindow();

	public slots:
        void importeraKontoutdrag();
        void importeraKontoplan();
        void skapaKonto();
        void visaKontoplan();
        void sparaBokforing();

	private:
		Ui::MainWindow *ui;
        Bokforing*      mBokforing;

};
#endif // MAINWINDOW_H
