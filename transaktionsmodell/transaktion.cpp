#include "transaktion.h"
#include "transaktionsrad.h"

Transaktion::Transaktion(QObject *parent)
	: QObject{parent}
{

}

QDate Transaktion::datum() const
{
	return mDatum;
}

QString Transaktion::transaktionsText() const
{
	return mTransaktionsText;
}

QList<TransaktionsRad *> Transaktion::transaktionsRader() const
{
	return mTransaktionsRader;
}

void Transaktion::rensaAllaTransaktionsRaderUtomForsta()
{
	while(mTransaktionsRader.size() > 1)
		delete mTransaktionsRader.takeLast();

	emit rowsChanged();

}

QString Transaktion::toString() const
{
	QStringList result;

	result << QString("Datum: %1").arg(mDatum.toString("yyyy-mm-dd"));
	result << QString("Text: %1").arg(mTransaktionsText);
	result << "Rader: ";

	foreach(TransaktionsRad* rad, mTransaktionsRader)
		result << rad->toString();

	return result.join('\n');

}

void Transaktion::setDatum(const QDate &newDatum)
{
	mDatum = newDatum;

	emit dateChanged(mDatum);
}

void Transaktion::setTransaktionsText(const QString &newTransaktionsText)
{
	mTransaktionsText = newTransaktionsText;

	emit descriptionChanged(mTransaktionsText);
}

void Transaktion::addTransaktionsRad(TransaktionsRad *nyRad)
{
	mTransaktionsRader.append(nyRad);
	emit rowsChanged();
}

void Transaktion::setTransaktionsRader(const QList<TransaktionsRad *> &newTransaktionsRader)
{
	while(mTransaktionsRader.size())
		delete mTransaktionsRader.takeLast();

	mTransaktionsRader = newTransaktionsRader;
	emit rowsChanged();
}

void Transaktion::setTransaktionsRaderUtomForsta(const QList<TransaktionsRad *> &newTransaktionsRader)
{
	while(mTransaktionsRader.size()  > 1)
		delete mTransaktionsRader.takeLast();

	mTransaktionsRader.append(newTransaktionsRader);

	emit rowsChanged();
}
