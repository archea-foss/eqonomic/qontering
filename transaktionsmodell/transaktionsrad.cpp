#include "transaktionsrad.h"

TransaktionsRad::TransaktionsRad(QObject *parent)
	: QObject{parent}
	, mKontonummer(0)
	, mBelopp(0)
	, mDebitKredit(Unknown)
{

}

TransaktionsRad::TransaktionsRad(KontoNummer aKontonummer,
                                 QString aRadtext,
                                 qreal aBelopp,
                                 TDebitKredit aDebitKredit,
                                 QObject *parent)
	: QObject(parent)
	, mKontonummer(aKontonummer)
	, mRadtext(aRadtext)
	, mBelopp(aBelopp)
	, mDebitKredit(aDebitKredit)
{


}

KontoNummer TransaktionsRad::kontonummer() const
{
	return mKontonummer;
}

void TransaktionsRad::setKontonummer(KontoNummer newKontonummer)
{
	mKontonummer = newKontonummer;
	emit kontoNummerChanged(mKontonummer);
}

QString TransaktionsRad::radtext() const
{
	return mRadtext;
}

void TransaktionsRad::setRadtext(const QString &newRadtext)
{
	mRadtext = newRadtext;
	emit radtextChanged(mRadtext);
}

qreal TransaktionsRad::belopp() const
{
	return mBelopp;
}

void TransaktionsRad::setBelopp(qreal newBelopp)
{
	mBelopp = newBelopp;
	emit beloppChanged(mBelopp);
}

TransaktionsRad::TDebitKredit TransaktionsRad::debitKredit() const
{
	return mDebitKredit;
}

QString TransaktionsRad::toString() const
{

	if (mDebitKredit == Debit)
		return QString("%1, %2, debit  , %3")
                .arg(mKontonummer.toInt(), 6)
				.arg(mRadtext, -30)
				.arg(mBelopp, 12, 'f', 2);
	else if (mDebitKredit == Kredit)
		return QString("%1, %2, kredit , %3")
                .arg(mKontonummer.toInt(), 6)
				.arg(mRadtext, -30)
				.arg(mBelopp, 12, 'f', 2);
	else
		return QString("%1, %2, unknown, %3")
                .arg(mKontonummer.toInt(), 6)
				.arg(mRadtext, -30)
				.arg(mBelopp, 12, 'f', 2);

}

void TransaktionsRad::setDebitKredit(TDebitKredit newDebitKredit)
{
	mDebitKredit = newDebitKredit;
	emit debitKreditChanged(mDebitKredit);

}
