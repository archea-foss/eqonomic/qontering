#ifndef TRANSAKTIONSRAD_H
#define TRANSAKTIONSRAD_H

#include <QObject>
#include <kontomodell/bokforingskonto.h>

class TransaktionsRad : public QObject
{
		Q_OBJECT

	public:

		enum TDebitKredit:int {Unknown = 0, Debit, Kredit};

	public:
		explicit TransaktionsRad(QObject *parent = nullptr);
        explicit TransaktionsRad(KontoNummer aKontonummer,
								 QString aRadtext,
								 qreal aBelopp,
								 TDebitKredit aDebitKredit,
								 QObject *parent = nullptr);

        KontoNummer kontonummer() const;

		QString radtext() const;

		qreal belopp() const;

		TDebitKredit debitKredit() const;

		QString toString() const;

	public slots:
        void setKontonummer(KontoNummer newKontonummer);
		void setRadtext(const QString &newRadtext);
		void setBelopp(qreal newBelopp);
		void setDebitKredit(TDebitKredit newDebitKredit);

	signals:

		void radtextChanged(QString newText);
		void beloppChanged(qreal belopp);
        void kontoNummerChanged(KontoNummer number);
		void debitKreditChanged(TDebitKredit debitKredit);

	private:
        KontoNummer         mKontonummer;
        QString             mRadtext;
        qreal               mBelopp;
        TDebitKredit        mDebitKredit;

};

#endif // TRANSAKTIONSRAD_H
