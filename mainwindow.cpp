#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <kontomodell/bokforingskonto.h>
#include <kontomodell/kontoplanlistmodell.h>
#include <kontomodell/kontoplantreemodell.h>
#include <kontomodell/transaktionsunderlag.h>
#include <kontomodell/transaktionsunderlag.h>
#include <kontomodell/transaktionsunderlagrad.h>

#include <QDate>
#include <QException>
#include <QFileDialog>
#include <QPushButton>
#include <QMessageBox>

#include <import/kontoplan-csv/importkontoplancsv.h>

#include <kontovyer/kontoeditdialog.h>
#include <kontovyer/kontoplanwidget.h>

#include <transaktionsvyer/transaktiongrundwidget.h>

#include <import/kontoutdrag-csv/importkontoutdragcsv.h>

#include <grundmodell/bokforing.h>

#include <exceptions/commonexception.h>


MainWindow::MainWindow(QWidget *parent)
	: QMainWindow(parent)
	, ui(new Ui::MainWindow)
{

	ui->setupUi(this);

	connect(ui->actionImportera_Kontoplan, &QAction::triggered, this, &MainWindow::importeraKontoplan);
	connect(ui->actionSkapa_konto, &QAction::triggered, this, &MainWindow::skapaKonto);

	connect(ui->actionImportera_Kontoutdrag, &QAction::triggered, this, &MainWindow::importeraKontoutdrag);
	connect(ui->actionVisa_Kontoplan, &QAction::triggered, this, &MainWindow::visaKontoplan);

    connect(ui->actionSpara, &QAction::triggered, this, &MainWindow::sparaBokforing);

    try {
        mBokforing = new Bokforing("data/acme/", this);

    }
    catch(CommonException &e)
    {
        QMessageBox::critical(this, "Misslyckades att öppna bokföringen",
                              QString("Undantag: %1\nFelinformation: %2\n%3")
                              .arg(e.exceptionType())
                              .arg(e.summary())
                              .arg(e.details()));
        exit(-1);

        return;

    }


//	ImportKontoplanCSV	import;
//    QList<Bokforingskonto *> importList = import.importera("underlag/Kontoplan-2023 - K2 Enkel v0.1.csv");
//    QList<Bokforingskonto *> importList = import.importera("underlag/Felsök-momskod.csv");
//	mKontoplan.importera(importList);

	QWidget*	 widget = new QWidget;
	QVBoxLayout* layout = new QVBoxLayout;

    KontoplanListmodell* kontoModell = new KontoplanListmodell(mBokforing->kontoplan());

	TransaktionGrundWidget* trans = nullptr;

	TransaktionsUnderlag*	underlag = new TransaktionsUnderlag();

	underlag->add(new TransaktionsUnderlagRad(QDate::fromString("2023-01-12", Qt::ISODate),"Egna domäner", -211.25, 1930));
	underlag->add(new TransaktionsUnderlagRad(QDate::fromString("2023-01-03", Qt::ISODate),"Företagsförsäkri", -1100.00, 1930));


	TransaktionsUnderlagRad	t1(QDate::fromString("2023-01-12", Qt::ISODate),"Egna domäner", -211.25, 1930);
	TransaktionsUnderlagRad	t2(QDate::fromString("2023-01-03", Qt::ISODate),"Företagsförsäkri", -1100.00, 1930);


	trans = new TransaktionGrundWidget(kontoModell);
    trans->setup(mBokforing, &t1);
	layout->addWidget(trans);

	trans = new TransaktionGrundWidget(kontoModell);
    trans->setup(mBokforing, &t2);
	layout->addWidget(trans);

//	layout->addSpacerItem(new QSpacerItem(20, 20, QSizePolicy::Maximum, QSizePolicy::MinimumExpanding));

	layout->addStretch(100);

	widget->setLayout(layout);
	ui->pTransrader->setWidget(widget);


//	KontoplanTreemodell* kontotreemodell = new KontoplanTreemodell(&mKontoplan);
//	KontoPlanWidget* kontowidget = new KontoPlanWidget;

////	kontowidget->setup(&mKontoplan);
//	kontowidget->setup(kontotreemodell);

//	kontowidget->show();


}

MainWindow::~MainWindow()
{
	delete ui;
}

void MainWindow::importeraKontoutdrag()
{
	QString filnamn = QFileDialog::getOpenFileName(this, "Välj fil med kontoutdrag att importera", "CSV (*.csv)");
	ImportKontoutdragCSV	import;
	QList<TransaktionsUnderlagRad *>	importList = import.importera(filnamn);



	QWidget*	 widget = new QWidget;
	QVBoxLayout* layout = new QVBoxLayout;

	TransaktionGrundWidget* trans = nullptr;
    KontoplanListmodell* kontoModell = new KontoplanListmodell(mBokforing->kontoplan());


	foreach(TransaktionsUnderlagRad* underlag, importList)
	{
		trans = new TransaktionGrundWidget(kontoModell);
        trans->setup(mBokforing, underlag);
		layout->addWidget(trans);
	}

//	layout->addSpacerItem(new QSpacerItem(20, 20, QSizePolicy::Maximum, QSizePolicy::MinimumExpanding));
	layout->addStretch(100);

	widget->setLayout(layout);
	ui->pTransrader->setWidget(widget);

}

void MainWindow::importeraKontoplan()
{
    ImportKontoplanCSV	import;


    QString filnamn = QFileDialog::getOpenFileName(this, "Välj konto-fil att importera", "CSV (*.csv)");

    QList<Bokforingskonto *> importList = import.importera(filnamn);

    mBokforing->kontoplan()->importera(importList);
}

void MainWindow::skapaKonto()
{
    KontoEditDialog* dialog = new KontoEditDialog(this);

    connect(dialog, &KontoEditDialog::nyttKonto, mBokforing->kontoplan(), &Kontoplan::append);
    dialog->show();

}

void MainWindow::visaKontoplan()
{
	KontoPlanWidget*	widget = new KontoPlanWidget;
    KontoplanTreemodell* modell = new KontoplanTreemodell(mBokforing->kontoplan(), widget);

	widget->setup(modell);

	widget->show();

}

void MainWindow::sparaBokforing()
{
    mBokforing->saveTo("data/mirror-acme/");

}

