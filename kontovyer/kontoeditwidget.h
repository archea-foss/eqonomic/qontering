#ifndef KONTOEDITWIDGET_H
#define KONTOEDITWIDGET_H

#include <QWidget>

namespace Ui {
class KontoDetaljer;
}

class Bokforingskonto;

class KontoEditWidget : public QWidget
{
		Q_OBJECT

	public:
		explicit KontoEditWidget(QWidget *parent = nullptr);

		void setup(Bokforingskonto* konto);
		~KontoEditWidget();

	protected slots:
		void kontonummerChanged(const QString& text);
		void kontonamnChanged(const QString& text);

	private:
		Ui::KontoDetaljer	*ui;
		Bokforingskonto		*mKonto;

};

#endif // KONTOEDITWIDGET_H
