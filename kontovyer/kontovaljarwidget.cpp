#include "kontoplanwidget.h"
#include "kontovaljarwidget.h"
#include "ui_kontovaljarwidget.h"

#include <kontomodell/kontoplanlistmodell.h>
#include <kontomodell/kontoplantreemodell.h>


#include <QWidget>
#include <QCompleter>
#include <QMenu>
#include <QPoint>
#include <QRegularExpression>
#include <QRegularExpressionMatch>
#include <QtDebug>
#include <QAbstractItemView>

KontoValjarWidget::KontoValjarWidget(QWidget *parent) :
	QWidget(parent),
	ui(new Ui::KontoValjarWidget),
	mAktuelltKonto(Bokforingskonto::KontonummerNull),
	mAntalSynligaActions(0),
	mSenastSynligtKonto(Bokforingskonto::KontonummerNull)
{
	ui->setupUi(this);
    connect(ui->pKontoText, &LineEditWithEscape::textChanged, this, &KontoValjarWidget::nummerElleNamn);
    connect(ui->pKontoText, &LineEditWithEscape::userEscape, this, &KontoValjarWidget::cancleEdit);
}

KontoValjarWidget::~KontoValjarWidget()
{
	delete ui;
}

void KontoValjarWidget::setModel(KontoplanListmodell *model)
{
	mModell = model;

	if (mModell)
    {
        QCompleter *completer = new QCompleter(this);
        completer->setModel(mModell);
        completer->setCaseSensitivity(Qt::CaseInsensitive);
        completer->setCompletionRole(Qt::DisplayRole);
        completer->setFilterMode(Qt::MatchContains);
        ui->pKontoText->setCompleter(completer);

        connect(completer, QOverload<const QModelIndex &>::of(&QCompleter::activated),
                this, &KontoValjarWidget::anvandareValdeKonto);

    }
}

void KontoValjarWidget::nummerElleNamn(const QString &text)
{
    if (text.size() == 1)
    {
        if (text[0].isDigit())
            ui->pKontoText->completer()->setFilterMode(Qt::MatchStartsWith);
        else
            ui->pKontoText->completer()->setFilterMode(Qt::MatchContains);
    }
}

void KontoValjarWidget::anvandareValdeKonto(const QModelIndex &index)
{
    if (index.isValid())
    {
        mAktuelltKonto = index.data(KontoplanListmodell::KontoNummerRole).toInt();
        ui->pKontoText->completer()->popup()->close();
        emit kontoValt(mAktuelltKonto);
    }
}

void KontoValjarWidget::cancleEdit()
{
    if (ui->pKontoText->completer() && ui->pKontoText->completer()->popup())
        ui->pKontoText->completer()->popup()->close();

}

void KontoValjarWidget::focusInEvent(QFocusEvent *event)
{
    Q_UNUSED(event);
	ui->pKontoText->setFocus();
}

KontoNummer KontoValjarWidget::aktuelltKonto() const
{
	return mAktuelltKonto;
}
