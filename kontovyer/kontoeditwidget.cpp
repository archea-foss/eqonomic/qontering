#include "kontoeditwidget.h"
#include "ui_kontoeditwidget.h"

#include <kontomodell/bokforingskonto.h>


#include <QtDebug>

KontoEditWidget::KontoEditWidget(QWidget *parent)
	: QWidget(parent)
	, ui(new Ui::KontoDetaljer)
	, mKonto(nullptr)
{
	ui->setupUi(this);

	connect(ui->pEditKontonummer, &QLineEdit::textChanged, this, &KontoEditWidget::kontonummerChanged);
	connect(ui->pEditBenamning, &QLineEdit::textChanged, this, &KontoEditWidget::kontonamnChanged);

}

KontoEditWidget::~KontoEditWidget()
{
	delete ui;
}

void KontoEditWidget::kontonummerChanged(const QString &text)
{

	qDebug() << "Kontonummer ändrat...";
	if (mKonto)
	{

        KontoNummer nummer(text);
        if (nummer.isValid())
		{
			mKonto->setKontoNummer(nummer);
			qDebug() << "Uppdaterar kontonummer: " << mKonto->kontoNummer();
		}
	}
}

void KontoEditWidget::kontonamnChanged(const QString& text)
{
	qDebug() << "Kontonamn ändrat...";

	if (mKonto)
	{
		mKonto->setKontoNamn(text);
		qDebug() << "Uppdaterar kontonamnet: " << mKonto->kontoNamn();
	}

}

void KontoEditWidget::setup(Bokforingskonto *konto)
{
	if (konto)
	{
		mKonto = konto;
        ui->pEditKontonummer->setText(mKonto->kontoNummer().toString());
		ui->pEditBenamning->setText(mKonto->kontoNamn());
	}

}
