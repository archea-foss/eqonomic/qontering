#ifndef KONTOVALJARWIDGET_H
#define KONTOVALJARWIDGET_H

#include <QPointer>
#include <QWidget>

#include <kontomodell/bokforingskonto.h>


namespace Ui {
class KontoValjarWidget;
}

class QMenu;
class KontoPlanWidget;
class KontoplanListmodell;
class KontoValjarWidget : public QWidget
{
		Q_OBJECT

	public:
		explicit KontoValjarWidget(QWidget *parent = nullptr);
		~KontoValjarWidget();

        KontoNummer aktuelltKonto() const;

	public slots:
        void setModel(KontoplanListmodell *model);

	signals:
        void kontoValt(KontoNummer kontonummer);


	protected slots:

        void nummerElleNamn(const QString& text);
        void anvandareValdeKonto(const QModelIndex &index);

		void cancleEdit();

	protected:
		virtual void focusInEvent(QFocusEvent *event)	override;

	private:
		Ui::KontoValjarWidget				*ui;
        QPointer<KontoplanListmodell>		mModell;
        KontoNummer                         mAktuelltKonto;
		QList<QAction*>						mMenuActions;
		int									mAntalSynligaActions;
        KontoNummer                         mSenastSynligtKonto;
};

#endif // KONTOVALJARWIDGET_H
