#include "kontoeditdialog.h"
#include "ui_kontoeditdialog.h"

#include <kontomodell/bokforingskonto.h>

KontoEditDialog::KontoEditDialog(QWidget *parent) :
	QDialog(parent),
	ui(new Ui::KontoEditDialog)
{
	ui->setupUi(this);
	ui->pKontoEditor->setup(&mKonto);

	connect(ui->pBtnBox, &QDialogButtonBox::accepted, this, &KontoEditDialog::btnAcceptPressed);
}

KontoEditDialog::~KontoEditDialog()
{
	delete ui;
}

void KontoEditDialog::btnAcceptPressed()
{
	emit nyttKonto(&mKonto);

}
