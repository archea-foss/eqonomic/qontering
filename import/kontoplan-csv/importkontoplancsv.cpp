#include "importkontoplancsv.h"

#include <kontomodell/bokforingskonto.h>

#include <QFile>
#include <QTextStream>

#include <QtDebug>

ImportKontoplanCSV::ImportKontoplanCSV(QObject *parent)
	: QObject{parent}
{

}

QList<Bokforingskonto *> ImportKontoplanCSV::importera(const QString &importFileName)
{
	QList<Bokforingskonto *>	result;
	QFile						source(importFileName);

	if (source.open(QIODevice::Text  | QIODevice::ReadOnly))
	{
		QTextStream in(&source);
//		in.setCodec("ISO-8859-15");
        in.setCodec("UTF-8");

        in.readLine();      // Read and skip the header.

		while(!in.atEnd())
		{
			QStringList line = in.readLine().split(";");

			qDebug() << "Läst rad: " << line;

            if (line.size() == 6)
			{
				bool bOk = true;

                KontoNummer	nummer(line[0]);
                if (bOk)
                {
                    Bokforingskonto* nyttKonto = new Bokforingskonto(nummer, line[5]);

                    if (setKontoTyp(nyttKonto, line) &&
                        setMomsStatus(nyttKonto, line) &&
                        setMomsKod(nyttKonto, line))
                    {
                        result.append(nyttKonto);
                        qDebug() << "Skapade konto med nummer "
                                 << nyttKonto->kontoNummer()
                                 << ", med namn "
                                 << nyttKonto->kontoNamn()
                                 << ", av typen "
                                 << nyttKonto->kontoTyp()
                                 << ", med momsstatus "
                                 << nyttKonto->momsStatus()
                                 << ", med momskod"
                                 << nyttKonto->momsKod();



                    }
                    else
                    {
                        delete nyttKonto;
                        qDebug() << "Misslyckades att skapa konto baserat på: " << line;
                    }

                }

            }
        }

		qDebug() << "Import klar, importerade " << result.size() << " konton";
    }

    return result;
}

bool ImportKontoplanCSV::setKontoTyp(Bokforingskonto *konto, QStringList &line)
{
    if (line[1] == "Intäkt")
        konto->setKontoTyp(Bokforingskonto::Intakt);
    else if (line[1] == "Kostnad")
                   konto->setKontoTyp(Bokforingskonto::Kostnad);
    else if (line[1] == "Skuld")
        konto->setKontoTyp(Bokforingskonto::Skuld);
    else if (line[1] == "Tillgång")
        konto->setKontoTyp(Bokforingskonto::Tillgang);
    else
    {
        konto->setKontoTyp(Bokforingskonto::OspecificeradTyp);
        return false;
    }

    return true;

}

bool ImportKontoplanCSV::setMomsStatus(Bokforingskonto *konto, QStringList &line)
{
    if (line[2] == "" || line[2] == "Aldrig" || line[2] == "Ingen")
        konto->setMomsStatus(Bokforingskonto::IngenMoms);
    else if (line[2] == "Tillåts")
        konto->setMomsStatus(Bokforingskonto::KanHaMoms);
    else if (line[2] == "Alltid")
        konto->setMomsStatus(Bokforingskonto::AlltidMoms);
    else if (line[2] == "Låst")
        konto->setMomsStatus(Bokforingskonto::LastMoms);
    else
    {
        konto->setMomsStatus(Bokforingskonto::OspecificeradMoms);
        return false;
    }

    return true;

}

bool ImportKontoplanCSV::setMomsKod(Bokforingskonto *konto, QStringList &line)
{
    konto->setMomsKod(line[4]);

    return true;
}




