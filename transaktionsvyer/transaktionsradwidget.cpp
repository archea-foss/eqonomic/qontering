#include "transaktionsradwidget.h"
#include "ui_transaktionsradwidget.h"

#include <transaktionsmodell/transaktionsrad.h>

#include <kontomodell/kontoplanlistmodell.h>
#include <kontomodell/kontoplantreemodell.h>

class IgnoreUpdateManager
{
	public:
		explicit IgnoreUpdateManager(bool* b):
			mB(b)
		{
			*mB = true;
		}

		~IgnoreUpdateManager()
		{
			*mB = false;
		}

		bool* mB;

};

TransaktionsRadWidget::TransaktionsRadWidget(QWidget *parent) :
	QWidget(parent),
	ui(new Ui::TransaktionsRadWidget),
	bIgnoreUpdate(false),
	bIsLastRow(false)
{
	ui->setupUi(this);	

	connect(ui->pKonto, &KontoValjarWidget::kontoValt, this, &TransaktionsRadWidget::kontoEdited);
	connect(ui->pInfo, &QLineEdit::editingFinished, this, &TransaktionsRadWidget::radTextEdited);
	connect(ui->pKredit, &QLineEdit::editingFinished, this, &TransaktionsRadWidget::lastFieldEdited);
}

TransaktionsRadWidget::~TransaktionsRadWidget()
{
	delete ui;
}

void TransaktionsRadWidget::setup(TransaktionsRad *transaktionsRad, KontoplanListmodell *kontoplan)
{
	mTransaktionsRad = transaktionsRad;
	mKontoplan		 = kontoplan;

	publishTransaction();

	connect(this, &TransaktionsRadWidget::radtextChanged, mTransaktionsRad, &TransaktionsRad::setRadtext);
	connect(mTransaktionsRad, &TransaktionsRad::radtextChanged, this, &TransaktionsRadWidget::setRadtext);

	ui->pKonto->setModel(mKontoplan);
	connect(this, &TransaktionsRadWidget::kontoNummerChanged, mTransaktionsRad, &TransaktionsRad::setKontonummer);





}

void TransaktionsRadWidget::setIsLastRow(bool isLastRow)
{
	bIsLastRow = isLastRow;

}


void TransaktionsRadWidget::publishTransaction()
{
	ui->pInfo->setText(mTransaktionsRad->radtext());

	if (mTransaktionsRad->debitKredit() == TransaktionsRad::Debit)
		ui->pDebit->setText(QString::number(mTransaktionsRad->belopp(), 'f', 2));

	else if (mTransaktionsRad->debitKredit() == TransaktionsRad::Kredit)
		ui->pKredit->setText(QString::number(mTransaktionsRad->belopp(), 'f', 2));
}

void TransaktionsRadWidget::kontonummerSelected(int index)
{

}

void TransaktionsRadWidget::radTextEdited()
{

	if (mTransaktionsRad)
	{
		IgnoreUpdateManager	blocker(&bIgnoreUpdate);
		emit radtextChanged(ui->pInfo->text());
	}

}

void TransaktionsRadWidget::kontoEdited(KontoNummer kontonummer)
{
	if (!bIgnoreUpdate && mTransaktionsRad)
	{
		IgnoreUpdateManager blocker(&bIgnoreUpdate);
		emit kontoNummerChanged(kontonummer);
	}

}

void TransaktionsRadWidget::lastFieldEdited()
{
	if (bIsLastRow)
		emit addAnotherRow();
}

void TransaktionsRadWidget::setKontonummer(KontoNummer newKontonummer)
{
	if (!bIgnoreUpdate && mTransaktionsRad)
	{
		IgnoreUpdateManager blocker(&bIgnoreUpdate);

	}

}

void TransaktionsRadWidget::setRadtext(const QString &newRadtext)
{
	if (!bIgnoreUpdate)
	{
		ui->pInfo->setText(newRadtext);
	}

}

void TransaktionsRadWidget::setBelopp(qreal newBelopp)
{

}

void TransaktionsRadWidget::setDebitKredit(TransaktionsRad::TDebitKredit newDebitKredit)
{

}

void TransaktionsRadWidget::focusInEvent(QFocusEvent *event)
{
	Q_UNUSED(event)
	ui->pKonto->setFocus();
}
