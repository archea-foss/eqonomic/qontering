#ifndef TRANSAKTIONSRADWIDGET_H
#define TRANSAKTIONSRADWIDGET_H

#include <QPointer>
#include <QWidget>

#include <kontomodell/bokforingskonto.h>

#include <transaktionsmodell/transaktionsrad.h>

namespace Ui {
class TransaktionsRadWidget;
}

class TransaktionsRad;
class KontoplanListmodell;
class TransaktionsRadWidget : public QWidget
{
		Q_OBJECT

	public:
		explicit TransaktionsRadWidget(QWidget *parent = nullptr);
		~TransaktionsRadWidget();

        void setup(TransaktionsRad *transaktionsRad, KontoplanListmodell *kontoplan);
		void setIsLastRow(bool isLastRow);

	public slots:

		void publishTransaction();
		void kontonummerSelected(int index);
		void radTextEdited();
        void kontoEdited(KontoNummer kontonummer);

		void lastFieldEdited();


        void setKontonummer(KontoNummer newKontonummer);
		void setRadtext(const QString &newRadtext);
		void setBelopp(qreal newBelopp);
		void setDebitKredit(TransaktionsRad::TDebitKredit newDebitKredit);



	signals:
		void radtextChanged(QString newText);
		void beloppChanged(qreal belopp);
        void kontoNummerChanged(KontoNummer number);
		void debitKreditChanged(TransaktionsRad::TDebitKredit debitKredit);

		void addAnotherRow();

	protected:
		virtual void focusInEvent(QFocusEvent *event)	override;

	private:
		Ui::TransaktionsRadWidget			*ui;
		QPointer<TransaktionsRad>			mTransaktionsRad;
        QPointer<KontoplanListmodell>		mKontoplan;
		bool								bIgnoreUpdate;
		bool								bIsLastRow;
};

#endif // TRANSAKTIONSRADWIDGET_H
