#ifndef TRANSAKTIONSGRUNDRUBRIKWIDGET_H
#define TRANSAKTIONSGRUNDRUBRIKWIDGET_H

#include <QWidget>

namespace Ui {
class TransaktionsGrundRubrikWidget;
}

class TransaktionsGrundRubrikWidget : public QWidget
{
		Q_OBJECT

	public:
		explicit TransaktionsGrundRubrikWidget(QWidget *parent = nullptr);
		~TransaktionsGrundRubrikWidget();

	private:
		Ui::TransaktionsGrundRubrikWidget *ui;
};

#endif // TRANSAKTIONSGRUNDRUBRIKWIDGET_H
