#include "transaktiongrundwidget.h"
#include "transaktionsradwidget.h"
#include "ui_transaktiongrundwidget.h"

#include <kontomodell/kontoplanlistmodell.h>
#include <kontomodell/kontoplantreemodell.h>
#include <kontomodell/transaktionsunderlagrad.h>

#include <QTreeView>

#include <transaktionsmodell/transaktion.h>
#include <transaktionsmodell/transaktionsrad.h>

#include <momsmodell/momskod.h>
#include <momsmodell/momskoder.h>
#include <momsmodell/momskodlistmodell.h>

#include <QtDebug>

#include <grundmodell/bokforing.h>

TransaktionGrundWidget::TransaktionGrundWidget(KontoplanListmodell *kontoplan, QWidget *parent) :
	QWidget(parent),
	ui(new Ui::TransaktionGrundrad),
	mKontoplan(kontoplan),
    mMomskodmodell(nullptr),
	mTransaktion(nullptr)

{
	ui->setupUi(this);

//	ui->pKonto->setSizeAdjustPolicy(QComboBox::AdjustToContents);

	connect(ui->pBtnMer, &QToolButton::pressed, this, &TransaktionGrundWidget::skiftaSynligaDetaljer);
	connect(ui->pBtnOk, &QPushButton::pressed, this, &TransaktionGrundWidget::okPressed);

	ui->pKonto->setModel(mKontoplan);
	connect(ui->pKonto, &KontoValjarWidget::kontoValt, this, &TransaktionGrundWidget::kontoValt);

	connect(ui->pMomsKod, QOverload<int>::of(&QComboBox::activated), this, &TransaktionGrundWidget::momskodVald);

//	ui->pKonto->setCurrentIndex(23);
//	ui->pKonto->setCurrentText(ui->pKonto->itemData(23, Qt::DisplayRole).toString());
	ui->pDetailsWidget->setVisible(false);


}

TransaktionGrundWidget::~TransaktionGrundWidget()
{
	delete ui;
}

void TransaktionGrundWidget::skiftaSynligaDetaljer()
{
	if (ui->pDetailsWidget->isVisible())
	{
		ui->pDetailsWidget->setVisible(false);
//		resize(size().width(), 28);
	}
	else
	{
//		resize(size().width(), 487);
		ui->pDetailsWidget->setVisible(true);
	}
}

void TransaktionGrundWidget::setup(Bokforing* bokforing, TransaktionsUnderlagRad *underlag)
{	
	ui->pDatum->setDate(underlag->datum());
	ui->pInfo->setText(underlag->info());
	ui->pBelopp->setText(QString::number(underlag->belopp(), 'f', 2));

	if (mTransaktion)
		delete mTransaktion;

	mTransaktion = underlag->skapaTransaktion();

    ui->pTransaktionsRader->setup(mTransaktion, mKontoplan, mMomskodmodell);


    if (underlag->belopp() <= 0)
        mMomskodmodell = new MomskodListModell(bokforing->momskoder(), Momskoder::Inkop, this);
    else
        mMomskodmodell = new MomskodListModell(bokforing->momskoder(), Momskoder::Forsaljning, this);

    ui->pMomsKod->setModel(mMomskodmodell);

    uppdateraDetaljer();

}

void TransaktionGrundWidget::uppdateraVerifikationsrader()
{
//	qDebug() << "Gör om verifikationslistan";
//	QVBoxLayout* layout = new QVBoxLayout();

//	// Töm den gamla layouten och skapa om hela listan.
//	QLayout* oldLayout = ui->pTransaktionsRader->layout();
//	while(oldLayout && oldLayout->count())
//	{
//		QLayoutItem *widgetItem = oldLayout->takeAt(0);
//		if (widgetItem)
//		{
//			QWidget *widget = widgetItem->widget();

//			delete widgetItem;
//			delete widget;
//		}
//	}
//	delete oldLayout;


//	layout->setSpacing(0);

//	foreach(TransaktionsRad* rad, mTransaktion->transaktionsRader())
//	{
//		TransaktionsRadWidget* trans = new TransaktionsRadWidget(this);
//		trans->setup(rad);

//		layout->addWidget(trans);

//		qDebug() << "Adds a row";
//	}

////	layout->addStretch();

//	ui->pTransaktionsRader->setLayout(layout);
//	ui->pTransaktionsRader->updateGeometry();
//	ui->pDetailsWidget->updateGeometry();
//	this->updateGeometry();

}

void TransaktionGrundWidget::kontoValt(KontoNummer kontonummer)
{
    Q_ASSERT(mMomskodmodell != nullptr);
    Q_ASSERT(mKontoplan != nullptr);

	if (mKontoplan->kontoplan()->konto(kontonummer)->momsStatus() == Bokforingskonto::KanHaMoms)
	{
        int momsIndex = mMomskodmodell->indexForMomskod(mKontoplan->kontoplan()->konto(kontonummer)->momsKod());
        ui->pMomsKod->setCurrentIndex(momsIndex);
		momskodVald(momsIndex);
	}

	uppdateraDetaljer();

}

void TransaktionGrundWidget::momskodVald(int index)
{
    Q_ASSERT(mMomskodmodell != nullptr);

	qreal belopp = ui->pBelopp->text().toDouble();
    qreal momsSats = mMomskodmodell->momskodPerIndex(ui->pMomsKod->currentIndex())->procentsats();
	qreal beloppUtanMoms = belopp / (1+ momsSats / 100);
	qreal momsBelopp = belopp - beloppUtanMoms;

	ui->pMomsbelopp->setText(QString::number(momsBelopp, 'f', 2));

	uppdateraDetaljer();
}

void TransaktionGrundWidget::uppdateraDetaljer()
{
    KontoNummer kontonummer = ui->pKonto->aktuelltKonto();

	Transaktion::TransaktionsRader	transaktionsRader;

//	mTransaktion->rensaAllaTransaktionsRaderUtomForsta();

	if (mKontoplan &&
		mKontoplan->kontoplan() &&
		mKontoplan->kontoplan()->konto(kontonummer) &&
		mKontoplan->kontoplan()->konto(kontonummer)->momsStatus() == Bokforingskonto::KanHaMoms)
	{
		qreal belopp = ui->pBelopp->text().toDouble();
        qreal momsSats = mMomskodmodell->momskodPerIndex(ui->pMomsKod->currentIndex())->procentsats();
        KontoNummer momsKonto = mMomskodmodell->momskodPerIndex(ui->pMomsKod->currentIndex())->momskonto();

		qreal beloppUtanMoms = belopp / (1+ momsSats / 100);
		qreal momsBelopp = belopp - beloppUtanMoms;

        transaktionsRader.append(new TransaktionsRad(kontonummer, kontonummer.toString(), beloppUtanMoms , TransaktionsRad::Kredit));
//		mTransaktion->addTransaktionsRad(new TransaktionsRad(kontonummer, QString::number(kontonummer), beloppUtanMoms , TransaktionsRad::Kredit));
		if (momsBelopp >= 0.01 || momsBelopp <= -0.01)
            transaktionsRader.append(new TransaktionsRad(momsKonto, momsKonto.toString(), momsBelopp , TransaktionsRad::Kredit));
//			mTransaktion->addTransaktionsRad(new TransaktionsRad(momsKonto, QString::number(momsKonto), momsBelopp , TransaktionsRad::Kredit));

	}
	else
	{
//		mTransaktion->addTransaktionsRad(new TransaktionsRad(kontonummer, QString::number(kontonummer), ui->pBelopp->text().toDouble(), TransaktionsRad::Kredit));
        transaktionsRader.append(new TransaktionsRad(kontonummer, kontonummer.toString(), ui->pBelopp->text().toDouble(), TransaktionsRad::Kredit));
	}

	// This is an editor after all, so add a few extra empty rows at the end
	transaktionsRader.append(new TransaktionsRad());
	transaktionsRader.append(new TransaktionsRad());


//	uppdateraVerifikationsrader();
	mTransaktion->setTransaktionsRaderUtomForsta(transaktionsRader);

}

void TransaktionGrundWidget::okPressed()
{
	qDebug() << "Följande info finns för transaktionen:";
	qDebug().noquote() << mTransaktion->toString();

}
