#ifndef TRANSAKTIONSRADERWIDGET_H
#define TRANSAKTIONSRADERWIDGET_H

#include <QPointer>
#include <QWidget>

class Transaktion;
class KontoplanListmodell;
class MomskodListModell;

class TransaktionsRaderWidget : public QWidget
{
		Q_OBJECT
	public:
		explicit TransaktionsRaderWidget(QWidget *parent = nullptr);
        void setup(Transaktion* transaktion, KontoplanListmodell* kontoplan, MomskodListModell* momsplan);

	public slots:
		void rowsChanged();
		void anotherRowIsExpected();

	signals:

	private:
		QPointer<Transaktion>			mTransaktion;
        QPointer<KontoplanListmodell>	mKontoplan;
		QPointer<MomskodListModell>		mMomsplan;

};

#endif // TRANSAKTIONSRADERWIDGET_H
