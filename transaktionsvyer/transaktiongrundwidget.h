#ifndef TRANSAKTIONGRUNDWIDGET_H
#define TRANSAKTIONGRUNDWIDGET_H

#include <QWidget>

#include <kontomodell/bokforingskonto.h>

namespace Ui {
class TransaktionGrundrad;
}

class Bokforing;
class Transaktion;
class KontoplanListmodell;
class MomskodListModell;
class TransaktionsUnderlagRad;
class TransaktionGrundWidget : public QWidget
{
		Q_OBJECT

	public:
        explicit TransaktionGrundWidget(KontoplanListmodell *kontoplan, QWidget *parent = nullptr);
		~TransaktionGrundWidget();

	public slots:
		void skiftaSynligaDetaljer();
        void setup(Bokforing *bokforing, TransaktionsUnderlagRad* underlag);

		void uppdateraVerifikationsrader();

	protected slots:
        void kontoValt(KontoNummer kontonummer);
		void momskodVald(int index);
		void uppdateraDetaljer();
		void okPressed();

	private:
		Ui::TransaktionGrundrad *ui;
        KontoplanListmodell				*mKontoplan;
        MomskodListModell				*mMomskodmodell;
		Transaktion						*mTransaktion;

};

#endif // TRANSAKTIONGRUNDWIDGET_H
