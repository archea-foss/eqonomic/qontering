#ifndef MOMSKOD_H
#define MOMSKOD_H

#include <QObject>
#include <QPointer>
#include <kontomodell/bokforingskonto.h>
#include <gemensamt/serializable.h>
#include <kontering/konteringsmallmoms.h>

class Momskod : public QObject, public Serializable
{
		Q_OBJECT

		const int IngenMomsKod = -1;

	public:
		explicit Momskod(QObject *parent = nullptr);
        explicit Momskod(QVariant variant, QObject *parent = nullptr);
        explicit Momskod(const QString & kod,
						 const QString& benamning,
                         KonteringsmallMoms* mall,
						 QObject *parent = nullptr);


        const QString &kod() const;
		const QString &benanmning() const;
        KontoNummer momskonto() const;
        qreal procentsats() const;

		QString toString();

        QVariantHash serialize() const override;
        virtual void deserialize(QVariantHash object) override;
        static bool isThisObject(QVariantHash object);
        static Momskod* parse(QVariantHash object);

	private:

        QString 							mKod;
		QString								mBenanmning;
        QPointer<KonteringsmallMoms>        mKonteringsmall;

};

#endif // MOMSKOD_H
