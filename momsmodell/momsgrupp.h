#ifndef MOMSGRUPP_H
#define MOMSGRUPP_H

#include <QObject>
#include <QPointer>
#include "momskoder.h"

class Momskod;
class Momsgrupp : public QObject
{
        Q_OBJECT
    public:
        explicit Momsgrupp(const QString& gruppnamn, Momskoder::Momstyp momstyp, QObject *parent = nullptr);

        void append(Momskod* momskod);

        QString namn() const;
        void setNamn(const QString &newNamn);

        QList<QPointer<Momskod> > momskoder() const;

    signals:


    protected:
        QString                         mNamn;
        Momskoder::Momstyp              mMomstyp;
        QList< QPointer<Momskod> >      mMomskoder;

};

#endif // MOMSGRUPP_H
