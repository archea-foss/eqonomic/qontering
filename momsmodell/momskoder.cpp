#include "momsgrupp.h"
#include "momskod.h"
#include "momskoder.h"

#include <QFile>
#include <QJsonDocument>
#include <QJsonDocument>
#include <QList>


#include <QtDebug>

#include <exceptions/objectinitializationfailedexception.h>

Momskoder::Momskoder(QObject *parent)
	: QObject{parent}
{
}

Momskoder::Momskoder(const QString &filePath, QObject *parent)
    : QObject {parent}
    , Serializable {filePath}
{

    deserialize(readJsonDocument("Momskoder").toVariant().toHash());
}

KontoNummer Momskoder::motkontoForMoms() const
{
    return	mRedovisningskontoMoms;
}

int Momskoder::size(const Momstyp momstyp) const
{
    return modell(momstyp).size();
}

Momskod *Momskoder::momskodPerIndex(const Momskoder::Momstyp momstyp, int index)
{
    if ( (index >= 0)  && (index < modell(momstyp).size()))
        return modell(momstyp).at(index);
    else
        return nullptr;


}

Momskod *Momskoder::momskodPerKod(const Momskoder::Momstyp momstyp, const QString &momsKod)
{
    foreach(Momskod* kod, modell(momstyp))
	{

		if (kod && kod->kod() == momsKod)
			return kod;

	}

    return nullptr;
}

Momskod *Momskoder::momskodPerKod(const QString &momsKod)
{
    Momskod *momskod = momskodPerKod(Inkop, momsKod);

    if (!momskod)
        momskod = momskodPerKod(Forsaljning, momsKod);

    return momskod;

}

int Momskoder::indexForMomskod(const Momskoder::Momstyp momstyp, const QString& momskod)
{
    for(int i = 0; i < modell(momstyp).size(); i++)
	{
        if (modell(momstyp).at(i) && modell(momstyp).at(i)->kod() == momskod)
			return i;
	}

	return -1;

}

bool Momskoder::harMoms(const QString& momsKod)
{
    Momskod *momskod = momskodPerKod(momsKod);
    if (momskod && momskod->procentsats() > 0)
        return true;
    else
        return false;
}

qreal Momskoder::momsSats(const QString& momsKod)
{
    Momskod *momskod = momskodPerKod(momsKod);
    if (momskod)
        return momskod->procentsats();
    else
        return 0;
}

KontoNummer Momskoder::momsKonto(const QString& momsKod)
{

    Momskod *momskod = momskodPerKod(momsKod);
    if (momskod)
        return momskod->momskonto();

    return KontoNummer();
}

QVariantHash Momskoder::serialize() const
{
    QVariantHash    result;

    QVariantList    inkopsGrupper;

    foreach(Momsgrupp* momsgrupp, mMomsgrupperInkop)
    {
        QVariantHash    inkopsGrupp;
        inkopsGrupp.insert("Benämning", momsgrupp->namn());

        QVariantList    inkopsKoder;
        foreach(Momskod* momskod, momsgrupp->momskoder())
        {
            if (momskod)
            {
                inkopsKoder.append(momskod->serialize());
            }
        }

        inkopsGrupp.insert("Momskoder", inkopsKoder);

        inkopsGrupper.append(inkopsGrupp);

    }

    QVariantList    forsaljningsGrupper;

    foreach(Momsgrupp* momsgrupp, mMomsgrupperForsaljning)
    {
        QVariantHash    forsaljningGrupp;
        forsaljningGrupp.insert("Benämning", momsgrupp->namn());

        QVariantList    forsaljningsKoder;
        foreach(Momskod* momskod, momsgrupp->momskoder())
        {
            if (momskod)
            {
                forsaljningsKoder.append(momskod->serialize());
            }
        }

        forsaljningGrupp.insert("Momskoder", forsaljningsKoder);

        forsaljningsGrupper.append(forsaljningGrupp);

    }

    result.insert("Object", "Momskoder");
    result.insert("Inköp", inkopsGrupper);
    result.insert("Försäljning", forsaljningsGrupper);

    result.insert("Redovisningskonto för moms", mRedovisningskontoMoms.toVariant());


    return result;

}

void Momskoder::deserialize(QVariantHash object)
{

    if (isThisObject(object))
    {
        mRedovisningskontoMoms = KontoNummer(object.value("Redovisningskonto för moms"));

        // Typnivå, inköp
        foreach(QVariant momsgrupp, object.value("Inköp").toList())
        {
            QVariantHash    momsgruppH = momsgrupp.toHash();

            Momsgrupp* grupp = new Momsgrupp(momsgruppH.value("Benämning").toString(),
                                             Momskoder::Inkop,
                                             this);

            mMomsgrupperInkop.append(grupp);

            // Gruppnivå ignoreras i dagsläget.
            foreach(QVariant momskod, momsgruppH.value("Momskoder").toList())
            {

                Momskod* newMomskod = new Momskod(momskod);
                if (newMomskod)
                {
                    mMomskoder.append(newMomskod);
                    grupp->append(newMomskod);
                }
                else
                    throw ObjectInitializationFailedException("Kunde inte skapa en Momskod för Inköp av okänd anledning",
                                                              "Momskoder::deserialize(QVariantHash object)");
            }

        }

        // Typnivå, Försäljning
        foreach(QVariant momsgrupp, object.value("Försäljning").toList())
        {
            QVariantHash    momsgruppH = momsgrupp.toHash();

            Momsgrupp* grupp = new Momsgrupp(momsgruppH.value("Benämning").toString(),
                                             Momskoder::Forsaljning,
                                             this);

            mMomsgrupperForsaljning.append(grupp);

            // Gruppnivå ignoreras i dagsläget.
            foreach(QVariant momskod, momsgruppH.value("Momskoder").toList())
            {

                Momskod* newMomskod = new Momskod(momskod);
                if (newMomskod)
                {
                    mMomskoderForsaljning.append(newMomskod);
                    grupp->append(newMomskod);
                }
                else
                    throw ObjectInitializationFailedException("Kunde inte skapa en Momskod för Försäljning av okänd anledning",
                                                              "Momskoder::deserialize(QVariantHash object)");
            }

        }
    }
    else
        throw ObjectInitializationFailedException("Fel underlag.",
                                                  "Momskoder");
}

bool Momskoder::isThisObject(QVariantHash object)
{
    if (object.value("Object").toString() == "Momskoder")
        return true;

    return false;

}

void Momskoder::saveTo(const QString &filePath)
{
    qDebug() << "Sparar Momskoder under " << filePath;

    saveJsonDocument(QJsonDocument::fromVariant(serialize()),
                     filePath,
                     "Momskoder");
}

QList<QPointer<Momskod> > Momskoder::modell(const Momskoder::Momstyp momstyp) const
{
    if (momstyp == Inkop)
        return mMomskoder;
    else
        return mMomskoderForsaljning;

}
