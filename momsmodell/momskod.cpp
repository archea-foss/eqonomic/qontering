#include "momskod.h"

#include <QVariantHash>

#include <exceptions/objectinitializationfailedexception.h>

Momskod::Momskod(QObject *parent)
	: QObject{parent}
{

}

Momskod::Momskod(QVariant variant, QObject *parent)
    : QObject{parent}
{
    deserialize(variant.toHash());
}

Momskod::Momskod(const QString &kod, const QString &benamning, KonteringsmallMoms *mall, QObject *parent)
    : QObject{parent}
    , mKod(kod)
    , mBenanmning(benamning)
    , mKonteringsmall(mall)
{

}

const QString& Momskod::kod() const
{
    return mKod;
}

const QString &Momskod::benanmning() const
{
	return mBenanmning;
}

KontoNummer Momskod::momskonto() const
{
    // ToDo: Remove this method - it is no longer valid.
    return KontoNummer();
}

qreal Momskod::procentsats() const
{
    if (mKonteringsmall)
        return mKonteringsmall->procentsats();

    return 0;
}

QString Momskod::toString()
{
	return QString("%1, %2").arg(mKod).arg(mBenanmning);

}

QVariantHash Momskod::serialize() const
{
    QVariantHash    result;

    result.insert("Momskod", mKod);
    result.insert("Benämning", mBenanmning);

    if (mKonteringsmall)
        result.insert("Kontera", mKonteringsmall->serialize());

    return result;
}

void Momskod::deserialize(QVariantHash object)
{
    if (isThisObject(object))
    {
        mKod            = object.value("Momskod").toString();
        mBenanmning     = object.value("Benämning").toString();
        mKonteringsmall = KonteringsmallMoms::factory(object.value("Kontera").toHash());

        return;
    }
    else
        throw ObjectInitializationFailedException("Fel underlag för att skapa en Momskod",
                                                  "Momskod::deserialize(QVariantHash object)");

}

bool Momskod::isThisObject(QVariantHash object)
{
    if (object.contains("Momskod") && object.contains("Benämning"))
        return true;

    return false;

}

Momskod *Momskod::parse(QVariantHash object)
{
    if (Momskod::isThisObject(object))
    {
        Momskod* result = new Momskod(
                              object.value("Momskod").toString(),
                              object.value("Benämning").toString(),
                              KonteringsmallMoms::factory(object.value("Kontera").toHash()));

        return result;

    }

    return nullptr;

}
