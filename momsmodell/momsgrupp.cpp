#include "momsgrupp.h"
#include "momskod.h"


Momsgrupp::Momsgrupp(const QString &gruppnamn, Momskoder::Momstyp momstyp, QObject *parent)
    : QObject{parent}
    , mNamn{gruppnamn}
    , mMomstyp{momstyp}
{

}

void Momsgrupp::append(Momskod *momskod)
{
    mMomskoder.append(momskod);
}

QString Momsgrupp::namn() const
{
    return mNamn;
}

void Momsgrupp::setNamn(const QString &newNamn)
{
    mNamn = newNamn;
}

QList<QPointer<Momskod> > Momsgrupp::momskoder() const
{
    return mMomskoder;
}
