#ifndef KONTERINGSMALLMOMS_H
#define KONTERINGSMALLMOMS_H

#include "konteringsmall.h"
#include <gemensamt/serializable.h>

class KonteringsmallMoms : public Konteringsmall, public Serializable
{
        Q_OBJECT
    public:
        explicit KonteringsmallMoms(QObject *parent = nullptr);

        virtual QVariantHash serialize() const override = 0;
        void deserialize(QVariantHash object) override = 0;
        static bool isThisObject(QVariantHash object);


        // Factory för alla subklasser till KonteringsmallMoms.
        static KonteringsmallMoms* factory(QVariantHash object);

        qreal procentsats() const;

    protected:
        qreal           mProcentsats;
};

#endif // KONTERINGSMALLMOMS_H
