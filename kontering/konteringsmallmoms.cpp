#include "konteringmomsforsaljning.h"
#include "konteringsmallmoms.h"

#include "konteringmomsinkop.h"
#include "konteringmomsinkopomvand.h"
#include <exceptions/objectinitializationfailedexception.h>

#include <QtDebug>


KonteringsmallMoms::KonteringsmallMoms(QObject *parent)
    : Konteringsmall{parent}
    , mProcentsats {0.0}
{
}

QVariantHash KonteringsmallMoms::serialize() const
{
    QVariantHash    result;

    result.insert("Momssats", mProcentsats);

    return result;

}

void KonteringsmallMoms::deserialize(QVariantHash object)
{
    if (isThisObject(object))
    {
        bool bOk = true;
        mProcentsats       = object.value("Momssats").toReal(&bOk);

        if (!bOk)
            throw ObjectInitializationFailedException("Fel i underlaget för att skapa en Konteringsmall för Moms",
                                                  "KonteringsmallMoms::KonteringsmallMoms(QVariantHash object, QObject *parent)");

    }
    else
        throw ObjectInitializationFailedException("Fel underlag för att skapa en Konteringsmall för Moms",
                                              "KonteringsmallMoms::KonteringsmallMoms(QVariantHash object, QObject *parent)");



}

bool KonteringsmallMoms::isThisObject(QVariantHash object)
{
    if (object.contains("Momssats"))
        return true;

    return false;

}

KonteringsmallMoms *KonteringsmallMoms::factory(QVariantHash object)
{
    try {
        if (KonteringMomsInkop::isThisObject(object))
            return new KonteringMomsInkop(object);

        else if (KonteringMomsInkopOmvand::isThisObject(object))
            return new KonteringMomsInkopOmvand(object);

        else if (KonteringMomsForsaljning::isThisObject(object))
            return new KonteringMomsForsaljning(object);

    }
    catch(ObjectInitializationFailedException &e)
    {
        qDebug() << "Got exception when factyoring a KonteringsmallMoms:";
        qDebug() << e.errorInfo;
        qDebug() << e.scope;

        throw(e);
    }


    return nullptr;

}

qreal KonteringsmallMoms::procentsats() const
{
    return mProcentsats;
}
