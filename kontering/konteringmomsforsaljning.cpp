#include "konteringmomsforsaljning.h"
#include <QVariant>

#include <exceptions/objectinitializationfailedexception.h>

KonteringMomsForsaljning::KonteringMomsForsaljning(QVariantHash object, QObject *parent)
    : KonteringsmallMoms{parent}
{
    deserialize(object);
}

QVariantHash KonteringMomsForsaljning::serialize() const
{
    QVariantHash result = KonteringsmallMoms::serialize();

    result.insert("Mall", "Försäljning");
    result.insert("Utgående", mMomskonto);

    return result;

}

void KonteringMomsForsaljning::deserialize(QVariantHash object)
{
    if (isThisObject(object))
    {
        KonteringsmallMoms::deserialize(object);

        mMomskonto = KontoNummer(object.value("Utgående"));

        if (!mMomskonto.isValid())
            throw ObjectInitializationFailedException("Fel i underlaget för att skapa en KonteringMomsForsaljning",
                                                      "KonteringMomsForsaljning::deserialize(QVariantHash object)");
    }
    else
        throw ObjectInitializationFailedException("Fel underlag för att skapa en KonteringMomsForsaljning",
                                                  "KonteringMomsForsaljning::deserialize(QVariantHash object)");

}

bool KonteringMomsForsaljning::isThisObject(QVariantHash object)
{
    if (object.value("Mall") == "Försäljning"
            && object.contains("Utgående"))

        return true;

    return false;
}
