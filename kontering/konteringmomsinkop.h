#ifndef KONTERINGMOMSINKOP_H
#define KONTERINGMOMSINKOP_H

#include "konteringsmallmoms.h"
#include <kontomodell/kontonummer.h>
#include <gemensamt/serializable.h>

class KonteringMomsInkop : public KonteringsmallMoms
{
        Q_OBJECT
    public:
        explicit KonteringMomsInkop(QVariantHash object,
                                    QObject *parent = nullptr);

        virtual QVariantHash serialize() const override;
        void deserialize(QVariantHash object) override;
        static bool isThisObject(QVariantHash object);

    protected:
        KontoNummer     mMomskonto;

};

#endif // KONTERINGMOMSINKOP_H
