QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++17

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    exceptions/commonexception.cpp \
    exceptions/failedtoreadjsonfileexception.cpp \
    exceptions/fileerrorexception.cpp \
    exceptions/objectinitializationfailedexception.cpp \
    gemensamt-ui/lineeditwithescape.cpp \
    gemensamt/serializable.cpp \
    grundmodell/bokforing.cpp \
    grundmodell/organisation.cpp \
    import/kontoplan-csv/importkontoplancsv.cpp \
    import/kontoutdrag-csv/importkontoutdragcsv.cpp \
    kontering/konteringmomsforsaljning.cpp \
    kontering/konteringmomsinkop.cpp \
    kontering/konteringmomsinkopomvand.cpp \
    kontering/konteringsmall.cpp \
    kontering/konteringsmallmoms.cpp \
    kontomodell/bokforingskonto.cpp \
    kontomodell/kontonummer.cpp \
    kontomodell/kontoplan.cpp \
    kontomodell/kontoplanlistmodell.cpp \
    kontomodell/kontoplantreemodell.cpp \
    kontomodell/transaktionsunderlag.cpp \
    kontomodell/transaktionsunderlagrad.cpp \
    kontomodell/transaktionsunderlagtreemodel.cpp \
    kontovyer/kontoeditdialog.cpp \
    kontovyer/kontoeditwidget.cpp \
    kontovyer/kontoplanwidget.cpp \
    kontovyer/kontovaljarwidget.cpp \
    main.cpp \
    mainwindow.cpp \
    momsmodell/momsgrupp.cpp \
    momsmodell/momskod.cpp \
    momsmodell/momskoder.cpp \
    momsmodell/momskodlistmodell.cpp \
    transaktionsmodell/transaktion.cpp \
    transaktionsmodell/transaktionsrad.cpp \
    grundmodell/bokforingsar.cpp \
    transaktionsvyer/skapatransaktionwidget.cpp \
    transaktionsvyer/transaktiongrundwidget.cpp \
    transaktionsvyer/transaktionsgrundrubrikwidget.cpp \
    transaktionsvyer/transaktionsraderwidget.cpp \
    transaktionsvyer/transaktionsradwidget.cpp

HEADERS += \
    exceptions/commonexception.h \
    exceptions/failedtoreadjsonfileexception.h \
    exceptions/fileerrorexception.h \
    exceptions/objectinitializationfailedexception.h \
    gemensamt-ui/lineeditwithescape.h \
    gemensamt/serializable.h \
    grundmodell/bokforing.h \
    grundmodell/organisation.h \
    import/kontoplan-csv/importkontoplancsv.h \
    import/kontoutdrag-csv/importkontoutdragcsv.h \
    kontering/konteringmomsforsaljning.h \
    kontering/konteringmomsinkop.h \
    kontering/konteringmomsinkopomvand.h \
    kontering/konteringsmall.h \
    kontering/konteringsmallmoms.h \
    kontomodell/bokforingskonto.h \
    kontomodell/kontonummer.h \
    kontomodell/kontoplan.h \
    kontomodell/kontoplanlistmodell.h \
    kontomodell/kontoplantreemodell.h \
    kontomodell/transaktionsunderlag.h \
    kontomodell/transaktionsunderlagrad.h \
    kontomodell/transaktionsunderlagtreemodel.h \
    kontovyer/kontoeditdialog.h \
    kontovyer/kontoeditwidget.h \
    kontovyer/kontoplanwidget.h \
    kontovyer/kontovaljarwidget.h \
    mainwindow.h \
    momsmodell/momsgrupp.h \
    momsmodell/momskod.h \
    momsmodell/momskoder.h \
    momsmodell/momskodlistmodell.h \
    transaktionsmodell/transaktion.h \
    transaktionsmodell/transaktionsrad.h \
    grundmodell/bokforingsar.h \
    transaktionsvyer/skapatransaktionwidget.h \
    transaktionsvyer/transaktiongrundwidget.h \
    transaktionsvyer/transaktionsgrundrubrikwidget.h \
    transaktionsvyer/transaktionsraderwidget.h \
    transaktionsvyer/transaktionsradwidget.h

FORMS += \
    kontovyer/kontoeditdialog.ui \
    kontovyer/kontoeditwidget.ui \
    kontovyer/kontoplanwidget.ui \
    kontovyer/kontovaljarwidget.ui \
    mainwindow.ui \
    transaktionsvyer/Prototyp-Transkation.ui \
    transaktionsvyer/Transaktion-Prototyp2-.ui \
    transaktionsvyer/skapatransaktionwidget.ui \
    transaktionsvyer/transaktiongrundwidget.ui \
    transaktionsvyer/transaktionsgrundrubrikwidget.ui \
    transaktionsvyer/transaktionsradwidget.ui

TRANSLATIONS += \
    qontering_sv_SE.ts
CONFIG += lrelease
CONFIG += embed_translations

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target
