#include "objectinitializationfailedexception.h"

ObjectInitializationFailedException::ObjectInitializationFailedException()
{

}

ObjectInitializationFailedException::ObjectInitializationFailedException(const ObjectInitializationFailedException &other)
    : errorInfo{other.errorInfo}
    , scope {other.scope}
{

}

ObjectInitializationFailedException::ObjectInitializationFailedException(const QString &_errorInfo, const QString &_scope)
    : errorInfo{_errorInfo}
    , scope{_scope}
{

}

QException *ObjectInitializationFailedException::clone() const
{
    return new ObjectInitializationFailedException(*this);

}

void ObjectInitializationFailedException::raise() const
{
    throw *this;
}

QString ObjectInitializationFailedException::exceptionType() const
{
    return "ObjectInitializationFailedException";
}

QString ObjectInitializationFailedException::summary() const
{
    return QString("Misslyckades med att skapa %1.").arg(scope);
}

QString ObjectInitializationFailedException::details() const
{
    return errorInfo;


}
