#include "failedtoreadjsonfileexception.h"

FailedToReadJSONFileException::FailedToReadJSONFileException()
    : error {QJsonParseError::NoError}
    , offset {-1}
{

}

FailedToReadJSONFileException::FailedToReadJSONFileException(const QString &sContext, const QJsonParseError &pError)
    : context { sContext }
    , errorMessage {pError.errorString()}
    , error {pError.error}
    , offset {pError.offset}
{

}

FailedToReadJSONFileException::FailedToReadJSONFileException(const FailedToReadJSONFileException &other)
    : context { other.context }
    , errorMessage {other.errorMessage}
    , error {other.error}
    , offset {other.offset}
{

}

QException *FailedToReadJSONFileException::clone() const
{

    return new FailedToReadJSONFileException(*this);

}

void FailedToReadJSONFileException::raise() const
{
    throw *this;
}

QString FailedToReadJSONFileException::exceptionType() const
{
    return "FailedToReadJSONFileException";

}

QString FailedToReadJSONFileException::summary() const
{
    return QString("Misslyckades med att läsa %1 från JSON").arg(context);

}

QString FailedToReadJSONFileException::details() const
{
    return QString("Fel typ %1 med felinformationen %2\nOffset för felet är %3.")
            .arg(error).arg(errorMessage).arg(offset);

}
