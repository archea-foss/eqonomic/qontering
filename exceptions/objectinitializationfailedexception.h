#ifndef OBJECTINITIALIZATIONFAILEDEXCEPTION_H
#define OBJECTINITIALIZATIONFAILEDEXCEPTION_H

#include "commonexception.h"

class ObjectInitializationFailedException : public CommonException
{
    public:
        ObjectInitializationFailedException();
        ObjectInitializationFailedException(const ObjectInitializationFailedException& other);
        ObjectInitializationFailedException(
                const QString& _errorInfo,
                const QString& _scope);

        virtual ~ObjectInitializationFailedException() {}

        virtual QException* clone() const override;
        virtual void raise() const override;

        virtual QString exceptionType() const override;
        virtual QString summary() const override;
        virtual QString details() const override;




    public:
        QString     errorInfo;
        QString     scope;
};

#endif // OBJECTINITIALIZATIONFAILEDEXCEPTION_H
