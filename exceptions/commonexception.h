#ifndef COMMONEXCEPTION_H
#define COMMONEXCEPTION_H

#include <QException>

class CommonException : public QException
{
    public:

        virtual QString exceptionType() const = 0;
        virtual QString summary() const = 0;
        virtual QString details() const = 0;

};

#endif // COMMONEXCEPTION_H
