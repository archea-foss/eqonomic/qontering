#include "kontoplanlistmodell.h"

KontoplanListmodell::KontoplanListmodell(Kontoplan* kontoplan, QObject *parent)
	: QAbstractListModel(parent)
	, mKontoplan(kontoplan)
{
}

QVariant KontoplanListmodell::headerData(int section, Qt::Orientation orientation, int role) const
{
	Q_UNUSED(orientation)
	Q_UNUSED(role);

	if (section == 0 && role == Qt::DisplayRole)
		return QString("Konto");

	else return QVariant();
}

int KontoplanListmodell::rowCount(const QModelIndex &parent) const
{
	// For list models only the root node (an invalid parent) should return the list's size. For all
	// other (valid) parents, rowCount() should return 0 so that it does not become a tree model.
	if (parent.isValid())
		return 0;
	else
		return mKontoplan->size();
}

QVariant KontoplanListmodell::data(const QModelIndex &index, int role) const
{
	if (!index.isValid())
		return QVariant();

	Bokforingskonto* konto = mKontoplan->kontoPerIndex(index.row());

    if (konto && index.column() == 0)
	{
        if (role == Qt::DisplayRole)
            return QString("%1, %2").arg(konto->kontoNummer().toString()).arg(konto->kontoNamn());

        else if (role == KontoNummerRole)
            return konto->kontoNummer();

        else if (role == KontoTextRole)
            return konto->kontoNamn();
	}


	return QVariant();
}

KontoNummer KontoplanListmodell::kontonummerPerIndex(const QModelIndex &index)
{
	if (index.isValid())
	{
		Bokforingskonto* konto = mKontoplan->kontoPerIndex(index.row());

		if (konto)
		{
			if (index.column() == 0 )
				return konto->kontoNummer();
		}
	}

    return KontoNummer();

}

KontoNummer KontoplanListmodell::kontonummerPerIndex(int index)
{
	Bokforingskonto* konto = mKontoplan->kontoPerIndex(index);

	if (konto)
		return konto->kontoNummer();
    else
        return KontoNummer();
}

Kontoplan *KontoplanListmodell::kontoplan() const
{
    return mKontoplan;

}

void KontoplanListmodell::aboutToBeginInsertRows(const QModelIndex &parent, int first, int last)
{
	beginInsertRows(parent, first, last);

}

void KontoplanListmodell::HasInsertedRows()
{
	endInsertRows();

}
