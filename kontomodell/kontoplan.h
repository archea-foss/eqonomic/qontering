#ifndef KONTOPLAN_H
#define KONTOPLAN_H

#include "bokforingskonto.h"

#include <QAbstractListModel>
#include <QHash>
#include <QMap>
#include <QObject>
#include <QVariant>

class Kontoplan : public QObject
{
		Q_OBJECT
	public:
		explicit Kontoplan(QObject *parent = nullptr);

		bool add(Bokforingskonto* konto);
        Bokforingskonto *konto(KontoNummer nummer);
		void importera(QList<Bokforingskonto *> importList);

		int size() const;
		Bokforingskonto* kontoPerIndex(int index);

		QList<Bokforingskonto*> kontoEnligtFilter(const QString& filter);

		const QList<Bokforingskonto *> &kontoLista() const;

	public slots:
		void append(Bokforingskonto* konto);

	signals:
		void beginInsertRows(const QModelIndex &parent, int first, int last);
		void endInsertRows();


	private:
		void insertSortedToList(Bokforingskonto *nyttKonto);

	private:

        QMap<KontoNummer, Bokforingskonto*>		mKontoMap;
        QList<Bokforingskonto*>                 mKontoList;

};

#endif // KONTOPLAN_H
