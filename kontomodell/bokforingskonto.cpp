#include "bokforingskonto.h"

Bokforingskonto::Bokforingskonto(QObject *parent)
	: QObject{parent}
	, mKontoNummer(KontonummerNull)
    , mKontoTyp(OspecificeradTyp)
    , mMomsStatus(IngenMoms)
{

}

Bokforingskonto::Bokforingskonto(KontoNummer kontoNummer, QString kontoNamn, QObject *parent)
	: QObject{parent}
	, mKontoNummer(kontoNummer)
	, mKontoNamn(kontoNamn)
    , mKontoTyp(OspecificeradTyp)
	, mMomsStatus(IngenMoms)
{

}

Bokforingskonto::Bokforingskonto(const Bokforingskonto &other)
    : QObject{nullptr}
    , mKontoNummer(other.mKontoNummer)
	, mKontoNamn(other.mKontoNamn)
    , mKontoTyp(other.mKontoTyp)
    , mMomsStatus(other.mMomsStatus)
    , mMomsKod(other.mMomsKod)
{

}

QVariantHash Bokforingskonto::serialize() const
{

}

void Bokforingskonto::deserialize(QVariantHash object)
{

}

bool Bokforingskonto::isThisObject(QVariantHash object)
{
    return true;

}

KontoNummer Bokforingskonto::kontoNummer() const
{
	return mKontoNummer;
}

void Bokforingskonto::setKontoNummer(KontoNummer newKontoNummer)
{
	mKontoNummer = newKontoNummer;
}

QString Bokforingskonto::kontoNamn() const
{
	return mKontoNamn;
}

void Bokforingskonto::setKontoNamn(const QString &newKontoNamn)
{
	mKontoNamn = newKontoNamn;
}

QString Bokforingskonto::toString() const
{
    return QString("%1, %2").arg(mKontoNummer.toInt()).arg(mKontoNamn);

}

void Bokforingskonto::reset()
{
	mKontoNummer = KontonummerNull;
	mKontoNamn.clear();
}

Bokforingskonto::MomsStatus Bokforingskonto::momsStatus() const
{
	return mMomsStatus;
}

void Bokforingskonto::setMomsStatus(MomsStatus newMomsStatus)
{
    mMomsStatus = newMomsStatus;
}


QString Bokforingskonto::momsKod() const
{
	return mMomsKod;
}

void Bokforingskonto::setMomsKod(const QString &newMomsKod)
{
    mMomsKod = newMomsKod;
}

Bokforingskonto::KontoTyp Bokforingskonto::kontoTyp() const
{
    return mKontoTyp;
}

void Bokforingskonto::setKontoTyp(KontoTyp newKontoTyp)
{
    mKontoTyp = newKontoTyp;
}

