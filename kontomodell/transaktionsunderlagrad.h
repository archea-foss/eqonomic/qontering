#ifndef TRANSAKTIONSUNDERLAGRAD_H
#define TRANSAKTIONSUNDERLAGRAD_H

#include <QDate>
#include <QObject>

#include "bokforingskonto.h"

/**
 * @brief The TransaktionsUnderlag class beskriver ett importerat underlag till bokföringen.
 *
 * Typiskt ett resultat av att man inmporterar CSV-filer från bank eller skattekonto.
 *
 */

class Transaktion;
class TransaktionsUnderlagRad : public QObject
{
		Q_OBJECT
	public:
		explicit TransaktionsUnderlagRad(QObject *parent = nullptr);
		explicit TransaktionsUnderlagRad(const QDate &date,
									  const QString &info,
									  qreal belopp,
                                      KontoNummer kontonummer);

		QDate datum() const;
		void setDatum(const QDate &newDate);

		QString info() const;
		void setInfo(const QString &newInfo);

		qreal belopp() const;
		void setBelopp(qreal newBelopp);

        KontoNummer kontonummer() const;
        void setKontonummer(KontoNummer newKontonummer);

		Transaktion* skapaTransaktion();

	signals:

	private:
        QDate               mDatum;
        QString             mInfo;
        qreal               mBelopp;
        KontoNummer         mKontonummer;		// Kontot importen berör


};

#endif // TRANSAKTIONSUNDERLAGRAD_H
