#include "kontoplan.h"
#include "kontoplantreemodell.h"

KontoplanTreemodell::KontoplanTreemodell(Kontoplan* kontoplan, QObject *parent)
	: QAbstractItemModel(parent)
	, mKontoplan(kontoplan)
{
	connect(kontoplan, &Kontoplan::beginInsertRows, this, &KontoplanTreemodell::aboutToBeginInsertRows);
	connect(kontoplan, &Kontoplan::endInsertRows, this, &KontoplanTreemodell::HasInsertedRows);
}

QVariant KontoplanTreemodell::headerData(int section, Qt::Orientation orientation, int role) const
{
	Q_UNUSED(orientation)
	Q_UNUSED(role);

	if (section == 0 && role == Qt::DisplayRole)
		return QString("Kontonummer");
	else if (section == 1 && role == Qt::DisplayRole)
		return QString("Benämning");
	else return QVariant();
}

QModelIndex KontoplanTreemodell::index(int row, int column, const QModelIndex &parent) const
{
    Q_UNUSED(parent);
	return createIndex(row, column);
}

QModelIndex KontoplanTreemodell::parent(const QModelIndex &index) const
{
    Q_UNUSED(index);
	return QModelIndex();
}

int KontoplanTreemodell::rowCount(const QModelIndex &parent) const
{
	if (!parent.isValid())
		return mKontoplan->size();


	return 0;


}

int KontoplanTreemodell::columnCount(const QModelIndex &parent) const
{
	if (!parent.isValid())
		return 2;

	return 0;
}

QVariant KontoplanTreemodell::data(const QModelIndex &index, int role) const
{
	if (!index.isValid())
		return QVariant();

	Bokforingskonto* konto = mKontoplan->kontoPerIndex(index.row());

//	qDebug() << "Data för " <<index.row() << ", " <<  index.column() << " konto är null?" << (konto == nullptr);

	if (konto && role == Qt::DisplayRole)
	{
		if (index.column() == 0 )
            return konto->kontoNummer().toString();
		else if (index.column() == 1)
			return konto->kontoNamn();
	}

	return QVariant();
}

void KontoplanTreemodell::aboutToBeginInsertRows(const QModelIndex &parent, int first, int last)
{
	beginInsertRows(parent, first, last);

}

void KontoplanTreemodell::HasInsertedRows()
{
	endInsertRows();

}

Kontoplan* KontoplanTreemodell::kontoplan() const
{
	return mKontoplan;
}
