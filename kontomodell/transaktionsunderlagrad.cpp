#include "transaktionsunderlagrad.h"

#include <transaktionsmodell/transaktion.h>
#include <transaktionsmodell/transaktionsrad.h>

TransaktionsUnderlagRad::TransaktionsUnderlagRad(QObject *parent)
	: QObject{parent}
{

}

TransaktionsUnderlagRad::TransaktionsUnderlagRad(const QDate &date,
                                           const QString &info,
                                           qreal belopp,
                                           KontoNummer kontonummer)
	: mDatum(date)
	, mInfo(info)
	, mBelopp(belopp)
	, mKontonummer(kontonummer)
{

}

QDate TransaktionsUnderlagRad::datum() const
{
	return mDatum;
}

void TransaktionsUnderlagRad::setDatum(const QDate &newDate)
{
	mDatum = newDate;
}

QString TransaktionsUnderlagRad::info() const
{
	return mInfo;
}

void TransaktionsUnderlagRad::setInfo(const QString &newInfo)
{
	mInfo = newInfo;
}

qreal TransaktionsUnderlagRad::belopp() const
{
	return mBelopp;
}

void TransaktionsUnderlagRad::setBelopp(qreal newBelopp)
{
	mBelopp = newBelopp;
}

KontoNummer TransaktionsUnderlagRad::kontonummer() const
{
	return mKontonummer;
}

void TransaktionsUnderlagRad::setKontonummer(KontoNummer newKontonummer)
{
	mKontonummer = newKontonummer;
}

Transaktion *TransaktionsUnderlagRad::skapaTransaktion()
{
	Transaktion* resultat = new Transaktion;

	resultat->setDatum(mDatum);
	resultat->setTransaktionsText(mInfo);

    resultat->addTransaktionsRad(new TransaktionsRad(mKontonummer, mKontonummer.toString(), mBelopp, TransaktionsRad::Debit));

	return resultat;
}
