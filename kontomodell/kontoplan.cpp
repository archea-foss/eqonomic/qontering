#include "kontoplan.h"

#include "bokforingskonto.h"


#include <QRegularExpression>
#include <QtDebug>

Kontoplan::Kontoplan(QObject *parent)
	: QObject{parent}
{

}

bool Kontoplan::add(Bokforingskonto *konto)
{

    if (konto && konto->kontoNummer().isValid())
	{

        mKontoMap.insert(konto->kontoNummer(), konto);
		insertSortedToList(konto);

		return true;
	}


	return false;

}

Bokforingskonto *Kontoplan::konto(KontoNummer nummer)
{
    return mKontoMap.value(nummer.toInt(), nullptr);
}

void Kontoplan::importera(QList<Bokforingskonto *> importList)
{

	qDebug() << "Importerar inlästa konton till kontoplanen";
	QList<Bokforingskonto *> valideradImportLista;

	foreach(Bokforingskonto *nyttKonto, importList)
	{
		if (nyttKonto)
		{
            if (mKontoMap.contains(nyttKonto->kontoNummer().toInt()))
			{
                    qDebug() << "Skippar konto " << nyttKonto->kontoNummer() << " ( " << nyttKonto->kontoNamn() << " ) då det redan finns i kontoplanen";
			}
			else
			{
				valideradImportLista.append(nyttKonto);
                qDebug() << "Lägger till konto " << nyttKonto->kontoNummer() << " ( " << nyttKonto->kontoNamn() << " ) till kontoplanen";
			}

		}
	}

	qDebug() << "Faktisk import sker.....";
	foreach(Bokforingskonto *nyttKonto, valideradImportLista)
	{
		add(nyttKonto);
	}
}

int Kontoplan::size() const
{
	return mKontoList.size();
}

Bokforingskonto *Kontoplan::kontoPerIndex(int index)
{
	return mKontoList.at(index);
}

QList<Bokforingskonto *> Kontoplan::kontoEnligtFilter(const QString &filter)
{
	QList<Bokforingskonto*> result;

	QRegularExpression kollaNummer("^(\\d+)");
	QRegularExpressionMatch nummerMatch  = kollaNummer.match(filter);
	qDebug() << "Nummersök: " << nummerMatch.captured(1);

	if (!nummerMatch.captured(1).isEmpty())
	{
		int soktNummer = nummerMatch.captured(1).toUInt();
        int lowMatch = 0;
        int highMatch = 0;

		if (soktNummer < 10)
		{
			lowMatch = soktNummer * 1000;
			highMatch = (soktNummer + 1) * 1000;
		}
		else if (soktNummer < 100)
		{
			lowMatch = soktNummer * 100;
			highMatch = (soktNummer + 1) * 100;
		}
		else if (soktNummer < 1000)
		{
			lowMatch = soktNummer * 10;
			highMatch = (soktNummer + 1) * 10;
		}
		else if (soktNummer < 10000)
		{
			lowMatch = soktNummer;
			highMatch = soktNummer + 1;
		}

		foreach(Bokforingskonto* konto, mKontoList)
		{

            if ((konto->kontoNummer().toInt() >= lowMatch) && (konto->kontoNummer().toInt() < highMatch))
				result.append(konto);
		}
		qDebug() << "Antal konton som matchar: " << result.size();
	}
	else
	{
		foreach(Bokforingskonto* konto, mKontoList)
		{
			if (konto->kontoNamn().contains(filter, Qt::CaseInsensitive))
				result.append(konto);
		}

	}

	return result;

}

void Kontoplan::append(Bokforingskonto *konto)
{
	if (konto)
	{
		Bokforingskonto* nyttKonto = new Bokforingskonto(*konto);
		if (!add(nyttKonto))
			delete nyttKonto;
	}

}

void Kontoplan::insertSortedToList(Bokforingskonto *nyttKonto)
{
	if (nyttKonto)
	{

		bool bInserted = false;
		QList<Bokforingskonto*>::iterator i;
		int index = 0;

		for (i = mKontoList.begin(); i != mKontoList.end(); i++, index++)
		{
            if (*i && (*i)->kontoNummer() > nyttKonto->kontoNummer())
			{
				beginInsertRows(QModelIndex(), index, index + 1);
				mKontoList.insert(i, nyttKonto);
				bInserted = true;
				endInsertRows();
				break;
			}
		}

		if (!bInserted)
		{
			beginInsertRows(QModelIndex(), mKontoList.size(), mKontoList.size());
			mKontoList.append(nyttKonto);
			endInsertRows();
		}

	}

}

const QList<Bokforingskonto *> &Kontoplan::kontoLista() const
{
	return mKontoList;
}
