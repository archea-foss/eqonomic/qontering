#ifndef KONTONUMMER_H
#define KONTONUMMER_H

#include <QString>
#include <QVariant>


class KontoNummer
{
    public:
        KontoNummer();
        KontoNummer(const QVariant& kontonummer);
        KontoNummer(const QString& kontonummer);
        KontoNummer(const int kontonummer);
        KontoNummer(const KontoNummer& other);

        ~KontoNummer();

        bool isValid() const;


        KontoNummer& operator = (const int kontonummer);
        KontoNummer& operator = (const QVariant &kontonummer);
        KontoNummer& operator = (const KontoNummer &kontonummer);

        bool operator == (const int kontonummer);
        bool operator == (const QVariant &kontonummer);
        bool operator == (const KontoNummer &kontonummer);

        bool operator > (const KontoNummer &kontonummer) const;
        bool operator < (const KontoNummer &kontonummer) const;
        bool operator >= (const KontoNummer &kontonummer) const;
        bool operator <= (const KontoNummer &kontonummer) const;


        operator QVariant () const;
        QVariant toVariant() const;
        int      toInt() const;
        QString  toString() const;

        friend uint qHash(const KontoNummer& kontonummer);

    protected:

        int     mNummer;

};

#endif // KONTONUMMER_H
