#ifndef KONTOPLANLISTMODELL_H
#define KONTOPLANLISTMODELL_H

#include "kontoplan.h"

#include <QAbstractListModel>
#include <QPointer>

class KontoplanListmodell : public QAbstractListModel
{
        Q_OBJECT

    public:

        static const int KontoNummerRole = Qt::UserRole + 1;
        static const int KontoTextRole = Qt::UserRole + 2;

	public:
		explicit KontoplanListmodell(Kontoplan* kontoplan, QObject *parent = nullptr);

		// Header:
		QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;

		// Basic functionality:
		int rowCount(const QModelIndex &parent = QModelIndex()) const override;

		QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;

        KontoNummer kontonummerPerIndex(const QModelIndex &index);
        KontoNummer kontonummerPerIndex(int index);

        Kontoplan* kontoplan() const;

	protected slots:
		void aboutToBeginInsertRows(const QModelIndex &parent, int first, int last);
		void HasInsertedRows();

	private:

		QPointer<Kontoplan>		mKontoplan;
};

#endif // KONTOPLANLISTMODELL_H
