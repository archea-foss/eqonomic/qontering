#include "kontonummer.h"



uint qHash(const KontoNummer& kontonummer)
{
    return static_cast<uint>(kontonummer.mNummer);
}

KontoNummer::KontoNummer()
    : mNummer(-1)
{

}

KontoNummer::KontoNummer(const QVariant &kontonummer)
    : mNummer(kontonummer.toInt())
{

}

KontoNummer::KontoNummer(const QString &kontonummer)
    : mNummer(kontonummer.toInt())
{

}

KontoNummer::KontoNummer(const int kontonummer)
    : mNummer(kontonummer)
{

}

KontoNummer::KontoNummer(const KontoNummer &other)
    : mNummer(other.mNummer)
{

}

KontoNummer::~KontoNummer()
{
    // Nothing to do
}

bool KontoNummer::isValid() const
{
    if (mNummer >= 1000 && mNummer <= 9999)
        return true;
    else
        return false;
}

KontoNummer &KontoNummer::operator =(const int kontonummer)
{
    mNummer = kontonummer;
    return *this;

}

bool KontoNummer::operator ==(const int kontonummer)
{
    if (mNummer == kontonummer)
        return true;
    else
        return false;

}

bool KontoNummer::operator >(const KontoNummer &kontonummer) const
{
    return mNummer > kontonummer.mNummer;
}

bool KontoNummer::operator <(const KontoNummer &kontonummer) const
{
    return mNummer < kontonummer.mNummer;
}

bool KontoNummer::operator >=(const KontoNummer &kontonummer) const
{
    return mNummer >= kontonummer.mNummer;

}

bool KontoNummer::operator <=(const KontoNummer &kontonummer) const
{
    return mNummer <= kontonummer.mNummer;
}

bool KontoNummer::operator ==(const QVariant &kontonummer)
{
    if (mNummer == kontonummer.toInt())
        return true;
    else
        return false;

}
bool KontoNummer::operator ==(const KontoNummer &kontonummer)
{
    if (mNummer == kontonummer.mNummer)
        return true;
    else
        return false;
}


KontoNummer &KontoNummer::operator =(const QVariant& kontonummer)
{
    mNummer = kontonummer.toInt();
    return *this;
}
KontoNummer &KontoNummer::operator =(const KontoNummer &kontonummer)
{
    mNummer = kontonummer.mNummer;
    return *this;

}


KontoNummer::operator QVariant () const
{
    return QVariant(mNummer);
}

QVariant KontoNummer::toVariant() const
{
    return QVariant(mNummer);
}

int KontoNummer::toInt() const
{
    return mNummer;

}

QString KontoNummer::toString() const
{
    if (isValid())
        return QString::number(mNummer);
    else
        return "<Ogiltligt kontonummer>";
}
