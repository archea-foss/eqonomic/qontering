#ifndef BOKFORINGSKONTO_H
#define BOKFORINGSKONTO_H

#include <QObject>

#include <gemensamt/serializable.h>
#include "kontonummer.h"

class Bokforingskonto : public QObject, public Serializable
{
		Q_OBJECT

	public:
		static const int KontonummerNull = 0;

        enum KontoTyp:int { OspecificeradTyp = 0,
                              Intakt,
                              Kostnad,
                              Skuld,
                              Tillgang };

		enum MomsStatus:int {OspecificeradMoms = 0,
							 IngenMoms,
							 KanHaMoms,
							 AlltidMoms,
							 LastMoms };

	public:
		explicit Bokforingskonto(QObject *parent = nullptr);
        explicit Bokforingskonto(KontoNummer kontoNummer, QString kontoNamn, QObject *parent = nullptr);
		explicit Bokforingskonto(const Bokforingskonto& other);

        // Serializable
        QVariantHash serialize() const override;
        virtual void deserialize(QVariantHash object) override;
        static bool isThisObject(QVariantHash object);


        KontoNummer kontoNummer() const;
        void setKontoNummer(KontoNummer newKontoNummer);

		QString kontoNamn() const;
		void setKontoNamn(const QString &newKontoNamn);

		QString toString() const;

		void reset();

		MomsStatus momsStatus() const;
		void setMomsStatus(MomsStatus newMomsStatus);

        QString momsKod() const;
        void setMomsKod(const QString& newMomsKod);

        KontoTyp kontoTyp() const;
        void setKontoTyp(KontoTyp newKontoTyp);

    signals:

	private:
        KontoNummer     mKontoNummer;
		QString			mKontoNamn;
        KontoTyp        mKontoTyp;
		MomsStatus		mMomsStatus;
        QString 		mMomsKod;

};

#endif // BOKFORINGSKONTO_H
