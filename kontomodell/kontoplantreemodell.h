#ifndef KONTOPLANTREEMODELL_H
#define KONTOPLANTREEMODELL_H

#include <QAbstractItemModel>
#include <QPointer>

class Kontoplan;
class KontoplanTreemodell : public QAbstractItemModel
{
		Q_OBJECT

	public:
		explicit KontoplanTreemodell(Kontoplan* kontoplan, QObject *parent = nullptr);

		// Header:
		QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;

		// Basic functionality:
		QModelIndex index(int row, int column,
						  const QModelIndex &parent = QModelIndex()) const override;
		QModelIndex parent(const QModelIndex &index) const override;

		int rowCount(const QModelIndex &parent = QModelIndex()) const override;
		int columnCount(const QModelIndex &parent = QModelIndex()) const override;

		QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;

		Kontoplan* kontoplan() const;

	protected slots:
		void aboutToBeginInsertRows(const QModelIndex &parent, int first, int last);
		void HasInsertedRows();


	private:

		QPointer<Kontoplan>		mKontoplan;
};

#endif // KONTOPLANTREEMODELL_H
