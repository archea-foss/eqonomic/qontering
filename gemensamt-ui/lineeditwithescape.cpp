#include "lineeditwithescape.h"

#include <QKeyEvent>

LineEditWithEscape::LineEditWithEscape(const QString &contents, QWidget *parent)
	: QLineEdit(contents, parent)
{

}

LineEditWithEscape::LineEditWithEscape(QWidget *parent)
	: QLineEdit(parent)
{

}

void LineEditWithEscape::keyPressEvent(QKeyEvent *event)
{
	if (event && event->key() == Qt::Key_Escape)
	{
		emit userEscape();
	}
	else
	{
		QLineEdit::keyPressEvent(event);
	}
}
