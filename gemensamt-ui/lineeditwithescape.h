#ifndef LINEEDITWITHESCAPE_H
#define LINEEDITWITHESCAPE_H

#include <QLineEdit>
#include <QObject>

class LineEditWithEscape : public QLineEdit
{
		Q_OBJECT
	public:
		LineEditWithEscape(const QString &contents, QWidget *parent = nullptr);
		LineEditWithEscape(QWidget *parent = nullptr);

	signals:
		void userEscape();

	protected:
		virtual void keyPressEvent(QKeyEvent *event) override;
};

#endif // LINEEDITWITHESCAPE_H
